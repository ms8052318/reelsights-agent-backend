def process_paragraph(paragraph):
    class ContentObject:
        def __init__(self, type_value, content_value):
            self.type = type_value
            self.content = content_value
    # Split the paragraph into sentences
    sentences = paragraph.split('. ')

    # Extract the title from the first sentence
    title = sentences[0].strip()

    # Initialize the result list
    result = []

    # Create the title object
    title_obj = ContentObject('title', title)
    result.append(title_obj)

    # Process the remaining sentences
    content = ''
    for sentence in sentences[1:]:
        # Remove leading and trailing whitespace
        sentence = sentence.strip()

        # If the current content is empty or the current sentence can be added to it, append it to the content
        if not content or len(content) + len(sentence) <= 60:
            if content:
                content += ' '
            content += sentence
        else:
            # Start a new content object with the current content
            content_obj = ContentObject('content', content)
            result.append(content_obj)
            content = sentence

    # Add the last content object if there is any remaining content
    if content:
        content_obj = ContentObject('content', content)
        result.append(content_obj)

    return result